let mockData = [
{
	"name": "Jake",
	"password": "abc123"
},
{
	"name": "Mary",
	"password": "qwe123"
},
]

let express = require("express");

const app = express(); 

const port = 4000;

app.get("/home", (req,res) => {
	res.send("Welcome to home page")
})

app.get("/users", (req,res) => {
	res.send(mockData); 
})

app.delete("/delete-user", (req,res) => {
	res.send(mockData[1]); 
})


app.listen(port, () => console.log(`Server is running at port ${port}`));
